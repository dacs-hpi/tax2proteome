FROM continuumio/miniconda:latest
WORKDIR ../
COPY Docker/environment.yml ./
COPY Accession.py ./
COPY DatabaseClass.py ./
COPY DatabaseCleaner.py ./
COPY Download.py ./
COPY Output.py ./
COPY tax2proteome.py ./
COPY TaxonGraph.py ./
COPY WriteCustomDB.py ./
COPY version.py ./
COPY TestFile.py ./
COPY Docker/boot.sh ./
RUN chmod +x boot.sh
RUN conda env create -f environment.yml
RUN echo "source activate tax2proteome_env" > ~/.bashrc
ENV PATH /opt/conda/envs/tax2proteome_env/bin:$PATH
ENTRYPOINT ["./boot.sh"]
