import argparse
import logging.config
from Bio import SeqIO
from pathlib import Path
import gzip

logger = logging.getLogger(__name__)


class DatabaseCleaner:

    # modified https://biopython.org/wiki/Sequence_Cleaner
    @staticmethod
    def non_redundant(fasta_file, with_taxon_ID, gz=False):
        """
        :param fasta_file: fasta file to make non redundant
        :param with_taxon_ID: True for uniprot, False for NCBI, different delimiter in header
        """
        def read_sequences(seq_record):
            sequence = str(seq_record.seq).upper()
            if sequence not in sequences:
                sequences[sequence] = seq_record.description
            # If sequence is already in the dict, concatenate headers to the other one that is already in the hash table
            else:
                if not with_taxon_ID:
                    sequences[sequence] += '\x01' + seq_record.description
                else:
                    sequences[sequence] += '\x01' + seq_record.description

        logger.info('Start creating non redundant database.')
        sequences = {}
        # Using the Biopython fasta parse to read fasta input
        if gz:
            nr_file = fasta_file.parent / (fasta_file.stem.split('.')[0] + '_nr.fasta.gz')
            with gzip.open(fasta_file, "rt") as handle:
                for seq_record in SeqIO.parse(handle, "fasta"):
                    read_sequences(seq_record)
        else:
            nr_file = fasta_file.parent / (fasta_file.stem.split('.')[0] + '_nr.fasta')
            for seq_record in SeqIO.parse(str(fasta_file), "fasta"):
                read_sequences(seq_record)


        # Write non redundant sequences
        try:
            with open(str(nr_file), "w+") as output_file:
                # Just read the hash table and write on the file as a fasta format with line length 60
                for sequence in sequences:
                    # write header
                    output_file.write(">" + sequences[sequence] + "\n")
                    # write sequence
                    seq_parts = [sequence[i:i + 60] for i in range(0, len(sequence), 60)]
                    for seq in seq_parts:
                        output_file.write(seq + "\n")
        except OSError:
            logger.exception('Not able to write non redundant database.', exc_info=True)

        logger.info("Fasta database %s is now non redundant." % fasta_file.name)
        return nr_file

    # Reduce headers to accessionIDs separated by \x01. file unzipped
    @staticmethod
    def reduce_header(path_to_db):
        """
                :param path_to_db: ncbi fasta file to reduce headers
        """
        try:
            with open(path_to_db, "rt") as database:
                rh_path = str(Path(path_to_db).parents[0] / str(Path(path_to_db).stem + '_rh.fasta'))
                with open(rh_path, 'w') as out:
                    logger.info('Start reducing NCBI headers.')
                    for line in database:
                        if line.startswith('>'):
                            if '\x01' in line:
                                out.write('>' + '\x01'.join([hdr.split(' ')[0] for hdr in line[1:].strip().split('\x01')])+ '\n')
                            else:
                                accessionID = (line[1:].strip().split(' ')[0])
                                out.write('>' + accessionID + '\n')
                        else:
                            out.write(line)
            logger.info("Fasta database %s has now reduced headers." % (Path(path_to_db).name))

        except FileNotFoundError:
            logger.exception('Database %s not found. No reduction of NCBI database headers.' % path_to_db,
                             exc_info=True)

        return rh_path

    # Reduce headers to first accessionID
    @staticmethod
    def reduce_ncbi_headers_to_one_accession(path_to_db):
        """
                :param path_to_db: ncbi fasta file to reduce headers
        """
        try:
            with open(path_to_db, "rt") as database:
                path = str(Path(path_to_db).parents[0] / str(Path(path_to_db).stem + '_one_acc.fasta'))
                with open(path, 'w') as out:
                    logger.info('Start reducing NCBI headers to first accession.')
                    for line in database:
                        if line.startswith('>'):
                            accessionID = (line[1:].strip().split(' ')[0])
                            accessionID = accessionID.split('\x01')[0]
                            out.write('>' + accessionID + '\n')
                        else:
                            out.write(line)
            logger.info("Fasta database %s headers have now only one accession." % (Path(path_to_db).name))

        except FileNotFoundError:
            print('Database %s not found. No reduction of NCBI database headers.' % path_to_db)

        return path

    @staticmethod
    def add_reverse_complement(path_to_db):
        """
                :param path_to_db: path to fasta file
        """
        try:
            with open(path_to_db, "rt") as database:
                path = str(Path(path_to_db).parents[0] / str(Path(path_to_db).stem + '_concatenated_target_decoy.fasta'))
                with open(path, 'w') as out:
                    first_entry = True
                    last_acc = ''
                    logger.info('Start adding rerverse complement fasta entries.')
                    for line in database:
                        if line.startswith('>'):
                            # Searchgui: IllegalArgumentException: Non-standard Halobacterium (Max Planck) header passed.
                            # Expecting something like '>OExyz (OExyz) xxx xxx xxx', but was 'OEK63348.1'
                            # entry removed
                            if not first_entry: # and not last_acc.startswith('OEK63348.1'):
                                out.write('>' + last_acc + '\n')
                                out.write(seq + '\n')
                                out.write('>' + last_acc + '_REVERSED' + '\n')
                                out.write(seq[::-1] + '\n')
                            seq = ''
                            last_acc = line[1:].strip()
                            if last_acc.startswith('pir'):
                                last_acc = last_acc.split('|')[1]
                            elif '|' in last_acc:
                                last_acc = f"{last_acc.split('|')[0]}|{last_acc.split('|')[1]}_REVERSED|{''.join(last_acc.split('|')[2:])}-REVERSED"
                            first_entry = False
                        else:
                            seq = seq + line.strip()
            print("Finish generating reverse complement." % (Path(path_to_db).name))

        except FileNotFoundError:
            print('Database %s not found. No generating of reverse coplement entries.' % path_to_db)

        return path



