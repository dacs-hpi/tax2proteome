import re
import urllib
from datetime import date
from pathlib import Path

from Download import Download
from Output import Output
from TestFile import TestFile


class Databases:

    def __init__(self, path_to_db_folder, logger, db_type, path_to_database, output_path):
        self.url_dict = {
            "url_protaccession2taxID": 'https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz',
            "url_protaccession2taxID_md5": 'https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.gz.md5',
            "url_pdbaccession2taxID": 'https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/pdb.accession2taxid.gz',
            "url_pdbaccession2taxID_md5" : 'https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/accession2taxid/pdb.accession2taxid.gz.md5',
            "url_taxdump": 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz',
            "url_taxdump_md5": 'ftp://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz.md5',
            "url_database_ncbi": 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz',
            "url_database_md5_ncbi": 'ftp://ftp.ncbi.nlm.nih.gov/blast/db/FASTA/nr.gz.md5',
            #"url_database_swissprot" = 'ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz'
            #"url_database_trembl" = 'ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz'
            "url_uniprot_metadata": 'ftp://ftp.expasy.org/databases/uniprot/current_release/knowledgebase/complete/RELEASE.metalink',
            # Europe
            "url_database_swissprot": "ftp://ftp.expasy.org/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz",
            "url_database_trembl": "ftp://ftp.expasy.org/databases/uniprot/current_release/knowledgebase/complete/uniprot_trembl.fasta.gz"
        }
        self.db_dict_name = {'ncbi': self.url_dict["url_database_ncbi"].split('/')[-1],
                             'swissprot': self.url_dict["url_database_swissprot"].split('/')[-1],
                             'trembl': self.url_dict["url_database_trembl"].split('/')[-1],
                             'pdb_accs': self.url_dict["url_pdbaccession2taxID"].split('/')[-1],
                             'prot_accs': self.url_dict["url_protaccession2taxID"].split('/')[-1],}
        self.logger = logger
        self.db_type = db_type
        self.path_to_db_folder = self.determine_path_db_folder(path_to_db_folder)
        self.path_to_db, self.db_type, self.db_is_gz = self.determine_path_to_database_and_download(db_type, path_to_database)
        self.is_prot_gz = self.check_all_needed_files_are_present_in_db_folder_and_download(db_type)
        self.is_with_taxon_id = self.test_is_uniprot_format()
        self.output_path = output_path

    def read_ncbi_hash(self, url, logger):
        attempts = 0
        while attempts < 5:
            try:
                with urllib.request.urlopen(url) as f:
                    return f.read().split()[0].decode('ascii')
            except urllib.error.URLError:
                attempts += 1
                if attempts == 5:
                    logger.exception(
                        'Download md5 hash from %s for validation of the ncbi file failed.Program continues '
                        'without validation.' % url, exc_info=True)
                    return None

    # reads metadata to download files from uniprot, like md5-hash and db-version
    def read_uniprot_metadata(self, url, database, logger):
        attempts = 0
        while attempts < 5:
            try:
                with urllib.request.urlopen(url) as f:
                    metavalues_db = f.read().decode('ascii')
                hit_version = re.search("<version>(.*)</version>", metavalues_db)
                hit_hash = re.search('<file name="' + database + '">(.*\n){3} *<hash type="md5">(.*)</hash>',
                                     metavalues_db)
                version = hit_version.group(1) if hit_version else None
                md5 = hit_hash.group(2) if hit_hash else None
                return version, md5
            except urllib.error.URLError:
                attempts += 1
                if attempts == 5:
                    logger.exception(
                        'Download of the uniprot database version and md5 hash for validation of the file %s failed. \n'
                        'Program continues without validation.' % database, exc_info=True)
                    return None, None

    def check_db_exists(self, path_to_db_folder, name):
        path_to_db = Path(path_to_db_folder)/self.db_dict_name[name].split('.gz')[0]
        path_to_db_gz = Path(path_to_db_folder)/self.db_dict_name[name]
        path_to_db_exists = Output.check_files_exist([path_to_db])[0]
        path_to_db_gz_exists = Output.check_files_exist([path_to_db_gz])[0]
        return path_to_db_exists, path_to_db_gz_exists

    def download_database(self, db):
        self.logger.warning(f"{db} database does not exist does not exist under the path {self.path_to_db_folder} "
                            f"and will be downloaded.")
        # download taxdump file (best at the same day)
        if db == "taxdump":
            taxdump_md5 = self.read_ncbi_hash(self.url_dict["url_taxdump_md5"], self.logger)
            dwl_taxdb = Download(self.url_dict["url_taxdump"],
                                 self.path_to_db_folder / self.url_dict["url_taxdump"].split('/')[-1],
                                 taxdump_md5)
            dwl_taxdb.download()
            self.logger.debug('End download of taxdump.tar.gz')
        # download prot.accession2taxid.gz (only for ncbi) and check md5 hash
        if db == "prot_accs":
            md5_hash = self.read_ncbi_hash(self.url_dict["url_protaccession2taxID_md5"], self.logger)
            dwl_protaccession = Download(self.url_dict["url_protaccession2taxID"],
                                         self.path_to_db_folder / self.url_dict["url_protaccession2taxID"].split('/')[-1],
                                         md5=md5_hash)
            dwl_protaccession.download()
            self.logger.debug('End download from %s to location %s.' %
                              (self.url_dict["url_protaccession2taxID"],
                               str(self.path_to_db_folder / self.url_dict["url_protaccession2taxID"].split('/')[-1])))
        if db == "pdb_accs":
            md5_hash = self.read_ncbi_hash(self.url_dict["url_pdbaccession2taxID_md5"], self.logger)
            dwl_pdbaccession = Download(self.url_dict["url_pdbaccession2taxID"],
                                        self.path_to_db_folder / self.url_dict["url_pdbaccession2taxID"].split('/')[-1],
                                        md5=md5_hash)
            dwl_pdbaccession.download()
            self.logger.debug('End download from %s to location %s.'
                              % (self.url_dict["url_pdbaccession2taxID"],
                                 str(self.path_to_db_folder / self.url_dict["url_pdbaccession2taxID"].split('/')[-1])))
        if db == 'ncbi':
            database_version_ncbi = 'ncbi ' + str(date)
            md5_hash = self.read_ncbi_hash(self.url_dict["url_database_md5_ncbi"], self.logger)
            dwl_db = Download(self.url_dict["url_database_ncbi"], self.path_to_db_folder / self.db_dict_name['ncbi'], md5=md5_hash)
            dwl_db.download()
            self.logger.debug("Databaseversion: %s" % database_version_ncbi)

        if db == 'swissprot':
            database_version_swissprot, hash_swissprot = self.read_uniprot_metadata(self.url_dict["url_uniprot_metadata"],
                                                                                    self.db_dict_name['swissprot'], self.logger)
            self.logger.debug("Database version swissprot: %s " % database_version_swissprot)
            dwl_db_swiss = Download(self.url_dict["url_database_swissprot"], self.path_to_db_folder / self.db_dict_name['swissprot'],
                                    md5=hash_swissprot)
            dwl_db_swiss.download()
        if db == 'trembl':
            database_version_trembl, hash_trembl = self.read_uniprot_metadata(self.url_dict["url_uniprot_metadata"],
                                                                              self.db_dict_name['trembl'], self.logger)
            self.logger.debug("Databaseversion trembl: %s." % database_version_trembl)
            dwl_db_trembl = Download(self.url_dict["url_database_trembl"], self.path_to_db_folder / self.db_dict_name['trembl'], md5=hash_trembl)
            dwl_db_trembl.download()

    def check_all_needed_files_are_present_in_db_folder_and_download(self, db_type):
        is_prot_gz = True
        taxdump_exists = Output.check_files_exist([self.path_to_db_folder / self.url_dict["url_taxdump"].split('/')[-1]])[0]
        if not taxdump_exists:
            self.download_database('taxdump')
        if db_type == 'ncbi':
            pdb_exists, pdb_gz_exists = self.check_db_exists(self.path_to_db_folder, 'pdb_accs')
            prot_exists, prot_gz_exists = self.check_db_exists(self.path_to_db_folder, 'prot_accs')
            if not pdb_exists and not pdb_gz_exists:
                self.download_database('pdb_accs')
            if not prot_exists and not prot_gz_exists:
                self.download_database('prot_accs')
            is_prot_gz = True if not prot_exists else False
        return is_prot_gz

    def determine_path_db_folder(self, path_to_db_folder):
        if not path_to_db_folder:
            path_to_db_folder = Path.cwd() / ('databases_' + str(date.today()))
        else:
            path_to_db_folder = Path.cwd() / path_to_db_folder
        if not path_to_db_folder.exists():
            try:
                path_to_db_folder.mkdir()
                self.logger.info("Downloaded databases are saved in direction %s" % path_to_db_folder)
            except FileExistsError:
                self.logger.debug("Database folder %s already exists. Checking for content." % path_to_db_folder)
            except OSError:
                self.logger.exception("No permission to create new database folder.", exc_info=True)
                exit(1)
        return path_to_db_folder

    def determine_path_to_database_and_download(self, db_type, path_to_database):
        if path_to_database:
            # user given path to database
            # given path to database checked, if not exists quit. Check if DB is in uniprot or ncbi format
            path_to_db = Path.cwd() / path_to_database
            user_db_exists = Output.check_files_exist([path_to_db])[0]
            if not user_db_exists:
                self.logger.error("Given database %s does not exist. Enter correct path under option --path_to_database. "
                                  "Program quits."
                                  % path_to_db)
                exit(1)
            # Test db type, if not header of uniprot type, db_type = 'ncbi'
            if not TestFile.test_uniprot(path_to_database):
                db_type = 'ncbi'
            db_is_gz = TestFile.test_gzipped(path_to_db)
            return path_to_db, db_type, db_is_gz

        else:
            # special treatment uniprot --> need swissprot and trembl database
            if db_type == 'uniprot':
                path_to_db = Path(self.path_to_db_folder)/'uniprot'
                path_to_trembl_exists, path_to_trembl_gz_exists = self.check_db_exists(self.path_to_db_folder, 'trembl')
                path_to_swissprot_exists, path_to_swissprot_gz_exists = self.check_db_exists(self.path_to_db_folder, 'swissprot')
                if not path_to_trembl_exists and not path_to_trembl_gz_exists:
                    self.download_database('trembl')
                    path_to_trembl_gz_exists = True
                if not path_to_swissprot_exists and not path_to_swissprot_gz_exists:
                    self.download_database('swissprot')
                    path_to_swissprot_gz_exists = True
                db_trembl_is_gz  = path_to_trembl_gz_exists if not path_to_trembl_exists else False
                db_swiss_is_gz = path_to_swissprot_gz_exists if not path_to_swissprot_exists else False
                return path_to_db, db_type, (db_trembl_is_gz, db_swiss_is_gz)
            #ncbi, swissprot, trembl
            else:
                path_to_db = Path(self.path_to_db_folder)/self.db_dict_name[db_type].split('.gz')[0]
                path_to_db_gz = Path(self.path_to_db_folder)/self.db_dict_name[db_type]
                path_to_db_exists = Output.check_files_exist([path_to_db])[0]
                path_to_db_gz_exists = Output.check_files_exist([path_to_db_gz])[0]
                if not path_to_db_exists and not path_to_db_gz_exists:
                    self.download_database(db_type)
                    path_to_db_gz_exists = True
                if path_to_db_exists:
                    return path_to_db, db_type, False
                elif path_to_db_gz_exists:
                    return path_to_db_gz, db_type, True

    def test_is_uniprot_format(self):
        if self.db_type == 'uniprot':
            db_trembl_is_gz, db_swiss_is_gz = self.db_is_gz
            if not db_swiss_is_gz:
                with_taxon_ID = TestFile.test_uniprot \
                    (self.path_to_db_folder/Path(self.db_dict_name['swissprot'].split('.gz')[0]))
            if db_swiss_is_gz:
                with_taxon_ID = TestFile.test_uniprot \
                    (self.path_to_db_folder/Path(self.db_dict_name['swissprot']))
        else:
            with_taxon_ID = TestFile.test_uniprot(self.path_to_db)
        return with_taxon_ID

    @staticmethod
    def determine_output_path(out):
        # if not option out, a new folder with name taxon_database and date for result database and log file is created
        if out:
            output_path = Path.cwd() / out
        else:
            output_path = Output.createDir(Path.cwd())
        return output_path
