"""
#
#-----------------------------
#  Robert Koch Institut
#         2019
#-----------------------------
# Author:
#
# Juliane Schmachtenberg
#
#------------------------------
"""

import re
import argparse
import logging
import pickle
import subprocess
from pathlib import Path

from DatabaseClass import Databases
from TaxonGraph import TaxonGraph
from DatabaseCleaner import DatabaseCleaner
from Accession import Accession
from TestFile import TestFile
from WriteCustomDB import WriteCustomDB
from version import __version__


# Initializing logger, log file saved in direction output_path = same as database
def initialize_logger(output_path, verbose=None):
    logpath = output_path.parents[0]
    logfile_name = str(output_path.stem + '.log')
    try:
        Path(logpath / logfile_name).unlink()
    except FileNotFoundError:
        pass
    if verbose:
        logging.basicConfig(format='%(asctime)s | %(name)s | %(levelname)s: %(message)s',
                            level=logging.DEBUG,
                            handlers=[logging.FileHandler("{0}/{1}".format(logpath, logfile_name)), logging.StreamHandler()])
    else:
        h1 = logging.FileHandler("{0}/{1}".format(logpath, logfile_name))
        h1.setLevel(logging.DEBUG)
        h2 = logging.StreamHandler()
        h2.setLevel(logging.INFO)
        logging.basicConfig(format='%(asctime)s | %(name)s | %(levelname)s: %(message)s',
                            level=logging.DEBUG,
                            handlers=[h1, h2])

    return logging.getLogger(__name__)


def read_taxids_from_file(taxID_input, input, column, logger):
    try:
        with open(input, 'r') as inputFile:
            for i, line in enumerate(inputFile):
                fields = line.rstrip('\r\n').split('\t')
                if len(fields) >= abs(column):
                    taxID = fields[column].strip()
                    if taxID.isdigit():
                        taxID_input.add(int(taxID))
                    else:
                        logger.error('Value %s in line %i of taxon input file is not a number. '
                                     'Right column number specified?' % (taxID, i))
                        continue
                else:
                    logger.error('Column number is bigger as number of columns in taxon ID input file. '
                                 'Program continues without taxon IDs from input file.')
    except FileNotFoundError:
        logger.exception('Taxon ID input file does not exist under specified path.', exc_info=True)
    return taxID_input


def read_config(path_to_config, options):
    try:
        with open(str(path_to_config), 'r') as config:
            param_dict = {}
            for line in config.readlines():
                if line.startswith('#'):
                    continue
                else:
                    try:
                        param_dict[re.search('<(.+?)>', line).group(1)] = line.split('\t')[-1].strip()
                    except IndexError:
                        param_dict[re.search('<(.+?)>', line).group(1)] = 'None'
    except FileNotFoundError:
        print('Config File ' + options.config + ' not found.')
        exit(1)

    try:
        options.database_folder = Path(param_dict['database_folder']) if param_dict['database_folder'] != 'None' else options.database_folder
        options.database = param_dict['database'] if param_dict['database'] in ['ncbi', 'uniprot', 'swissprot', 'trembl'] else options.database
        options.path_to_database = param_dict['path_to_database'] if param_dict['path_to_database'] != 'None' else options.path_to_database
        options.taxon = [[int(taxid) for taxid in param_dict['taxon'].split()]] if param_dict['taxon'] != 'None' else options.taxon
        options.level = param_dict['level'] if param_dict['level'] != 'None' else options.level
        options.out = Path(param_dict['out']) if param_dict['out'] != 'None' else options.out
        options.threads = int(param_dict['threads']) if param_dict['threads'] != 'None' else options.threads
        options.species = bool(int(param_dict['species'])) if param_dict['species'] != 'None' else options.species
        options.reduced_headers = bool(int(param_dict['reduce_header'])) if param_dict['reduce_header'] != 'None' else options.reduce_header
        options.non_redundant = bool(int(param_dict['non_redundant'])) if param_dict['non_redundant'] != 'None' else options.non_redundant
        options.no_descendants = bool(int(param_dict['no_descendants'])) if param_dict['no_descendants'] != 'None' else options.no_descendants
        options.gzip = bool(int(param_dict['gzip'])) if param_dict['gzip'] != 'None' else options.gzip
        options.input = bool(int(param_dict['input'])) if param_dict['input'] != 'None' else options.input
        options.verbose = bool(int(param_dict['verbose'])) if param_dict['verbose'] != 'None' else options.verbose
    except KeyError:
        print('Config file is incomplete. Missing parameter: ')
        exit(1)


# check if commandline input is positive integer
def positive_integer(value):
    ivalue = int(value)
    if ivalue < 0:
        raise argparse.ArgumentTypeError("%s is an invalid positive int value." % value)
    return ivalue


def main():
    parser = argparse.ArgumentParser(description='Find all protein database entrys of specified taxon IDs and their descendants.' \
                                                 ' One taxID or a taxID input file must be provided. Peptide-Databases from NCBI or Uniprot can be used. User defined databases,' \
                                                 ' if header contain taxon IDs (e.g. OX=1111) or ncbi/uniprot accession IDs.')
    parser.add_argument('-i', '--input', dest='input', default=None, help='TaxID input file: tabular file containing a column of NCBI'
                                                                          ' taxon IDs. Columns tab separated.')
    parser.add_argument('-c', '--column', dest='column', type=positive_integer, default=0, help='The column (zero-based) in the tabular '
                                                                                                'file that contains Taxon IDs. Default = 0.')
    parser.add_argument('-t', '--taxon', dest='taxon', type=positive_integer, nargs='+', action='append',
                        help='NCBI taxon ID/s for database extraction. Multiple taxonIDs seperated by space.')
    parser.add_argument('-d', '--database', dest='database', choices=['ncbi', 'uniprot', 'swissprot', 'trembl'],
                        default='uniprot', help='Database choice for analysis or for download. Choices: ncbi, uniprot, tremble, swissprot. '
                                                'No download, if databases with original name are stored in same folder as option --path ')
    parser.add_argument('-p', '--database_folder', dest='database_folder', default=None, help='Path to folder with all needed '
                                                                                              'databases: taxdump.tar.gz (for all databases), prot.accession2taxid or prot.accession2taxid.gz and '
                                                                                              'pdb.accession2taxid.gz (for ncbi databases). Optional: peptide_database named: nr/nr.gz, '
                                                                                              'uniprot_trembl.fasta/uniprot_trembl.fasta.gz or uniprot_sprot.fasta/uniprot_sprot.fasta.gz'
                                                                                              ' or uniprot.fasta./uniprot.fasta.gz')
    parser.add_argument('-o', '--out', dest='out', default=None,
                        help="File name and direction of the result taxon specified peptide database. "
                             "Default = /taxon_specified_db_DATE/taxon_specific_database.fasta")
    parser.add_argument('-n', '--path_to_database', dest='path_to_database', default=None,
                        help="Database name and direction. If database is in other folder than --database_folder or name deviates from standard names.")
    parser.add_argument('-l', '--level', dest='level',  choices=['species', 'section', 'genus', 'tribe', 'subfamily', 'family', 'superfamily',
                                                                 'order', 'superorder', 'class', 'phylum', 'kingdom', 'superkingdom'], default=None,
                        help='Hierarchy level up in anchestral tree. Choices: species, section, genus, tribe, '
                             'subfamily, family, superfamily, order, superorder, class, phylum, kingdom, superkingdom')
    parser.add_argument('-z', '--no_descendants', dest='no_descendants', action='store_true', default=False,
                        help='Select peptide database only by given taxon IDs, descendant taxons are excluded.')
    parser.add_argument('-s', '--species', dest='species', action='store_true', default=False,
                        help= "Select peptide database only until taxonomic level 'species', descendents from species are excluded.")
    parser.add_argument('-r', '--non_redundant', dest='non_redundant', action='store_true', default=False,
                        help='Makes the final database non redundant in regard to sequences, headers are concatenated.')
    parser.add_argument('-u', '--threads', dest='threads', type=positive_integer, action="store",
                        help='Number of threads for using multiprocessing. Default = number of cores.')
    parser.add_argument('-x', '--reduce_header', dest='reduce_header', action='store_true', default=False,
                        help='Reduce the long headers of NCBI entries to accession IDs. Use only for NCBI databases.')
    parser.add_argument('--gzip', dest='gzip', action='store_true', default=False,
                        help='Output database file gzip compressed, strong increase of running time')
    parser.add_argument('--config', dest='config', default=None,
                        help='Read config file containing DB_locations, taxon IDs, rank. Default = tax2proteome.config')
    parser.add_argument('--version', action='version', version=('version ' + __version__))
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False,
                        help='Verbose shows details about program progress and more information.')

    options = parser.parse_args()

    options.database = options.database.lower()
    # get options from config file
    if options.config:
        read_config(options.config, options)

    output_path = Databases.determine_output_path(options.out)
    logger = initialize_logger(output_path.parent / output_path.name, options.verbose)

    for arg, value in sorted(vars(options).items()):
        logger.debug("Argument %s: %r", arg, value)
    logger.debug("Result database and log file are saved in direction %s" % output_path)

    # if options.database_folder specified: folder to all databases (can be without protein DB if options.path_to_database)
    # if not exist, create folder with user defined name in option --path
    # valid db_path, download missing databases,
    database_obj = Databases(options.database_folder, logger, options.database, options.path_to_database, options.out)

    # Read taxIDs from option -t and option -i or from config
    if options.taxon:
        taxID_input = set([taxID for taxonlist in options.taxon for taxID in taxonlist])
    else:
        taxID_input = set()
    if options.input:
        taxID_input = read_taxids_from_file(taxID_input, options.input, options.column, logger)
    if not taxID_input:
        logger.error('No taxon ID given. Please check your input. Program quits. ')
        raise Exception('No taxon IDs.')

    logger.debug('Given Tax-IDs: %s' % ' '.join(str(it) for it in taxID_input))

    # Try load pre-builded taxonomy graph or built taxonomy graph now
    if not (database_obj.path_to_db_folder / 'taxon_graph').is_file():
        taxon_graph = TaxonGraph()
        logger.debug("Start building taxon graph.")
        taxon_graph.create_graph(database_obj.path_to_db_folder / database_obj.url_dict["url_taxdump"].split('/')[-1])
        logger.debug("Taxon graph successfully build.")
        # save TaxonGraph to harddrive:
        with open(str(database_obj.path_to_db_folder / 'taxon_graph'), 'wb') as handle:
            pickle.dump(taxon_graph, handle, protocol=pickle.HIGHEST_PROTOCOL)
            logger.debug('Safe taxon graph to location: %s' % str(database_obj.path_to_db_folder / 'taxon_graph'))
    # load Taxon Graph
    else:
        try:
            logger.debug('Load taxon graph.')
            with open(str(database_obj.path_to_db_folder / 'taxon_graph'), 'rb') as handle:
                taxon_graph = pickle.load(handle)
        except UnicodeDecodeError or EOFError:
            logger.exception(
                "Failed opening path to taxon graph / taxon_graph is corrupted. Delete %s file."
                % str(database_obj.path_to_db_folder / 'taxon_graph'))
            exit(1)

    # adjusts the hierarchy level, if level does not exist, take next smaller level
    if options.level:
        level = options.level
        logger.debug("Start selection of next ancestor of level %s for all given taxIDs" % level)
        taxIDs = {taxon_graph.find_level_up(taxID, level) for taxID in taxID_input}
        logger.info("All taxon IDs are set up to level %s in anchestral tree. Taxon IDs of level %s: %s"
                    % (level, level, ' '.join(str(it) for it in taxIDs)))
    else:
        taxIDs = taxID_input
    final_taxIDs = set()

    # find all descendants
    if not options.no_descendants:
        logger.debug("Start searching for all child taxon IDs.")
        for taxID in taxIDs:
            final_taxIDs.update(taxon_graph.find_taxIDs(taxID, options.species))
        logger.debug("End searching for all child taxon IDs.")
        logger.debug('Number of final taxon IDs: %s' % str(len(final_taxIDs)))
    else:
        final_taxIDs = taxIDs
        logger.debug('Number of taxon IDs for database search: %s' % str(len(final_taxIDs)))

    # generate accession_taxID dict for ncbi db search and write custom specified db to --out

    # ncbi
    if not database_obj.is_with_taxon_id:
        path_to_pdb_accs =  database_obj.path_to_db_folder / database_obj.url_dict["url_pdbaccession2taxID"].split('/')[-1]
        accession = Accession(final_taxIDs)
        logger.debug('Read accession files.')
        if database_obj.is_prot_gz:
            path_to_prot_accs = database_obj.path_to_db_folder / database_obj.db_dict_name["prot_accs"]
        else:
            path_to_prot_accs = database_obj.path_to_db_folder / database_obj.db_dict_name["prot_accs"].split('.gz')[0]
        accession.read_accessions(path_to_prot_accs, path_to_pdb_accs, options.threads)
        logger.debug('All accession IDs collected.')
        logger.info('Start writing taxon selected peptide database to %s.' % output_path)
        wc = WriteCustomDB(database_obj.path_to_db, output_path)
        wc.read_database(False, gzipped=TestFile.test_gzipped(database_obj.path_to_db), threads=options.threads,
                         accessions=accession.accessionIDs)
        logger.debug('End writing taxon selected peptide database.')

    # uniprotKB: write custom specified db to --out
    else:
        logger.info('Start writing taxon selected peptide database to %s.' % output_path)
        if database_obj.path_to_db.stem == 'uniprot':
            #TREMBL
            if database_obj.db_is_gz[0]:
                path_to_db = database_obj.path_to_db_folder / database_obj.db_dict_name['trembl']
            else:
                path_to_db = database_obj.path_to_db_folder / database_obj.db_dict_name['trembl'].split('.gz')[0]
            wc = WriteCustomDB(path_to_db, output_path, final_taxIDs)
            wc.read_database(True, threads=options.threads, gzipped=TestFile.test_gzipped(path_to_db))
            # SWISSPROT
            if database_obj.db_is_gz[1]:
                path_to_db = database_obj.path_to_db_folder / database_obj.db_dict_name['swissprot']
            else:
                path_to_db = database_obj.path_to_db_folder / database_obj.db_dict_name['swissprot'].split('.gz')[0]
            wc = WriteCustomDB(path_to_db, output_path, final_taxIDs)
            wc.read_database(True, threads=options.threads, gzipped=TestFile.test_gzipped(path_to_db), add=True)
        else:
            wc = WriteCustomDB(database_obj.path_to_db, output_path, final_taxIDs)
            wc.read_database(True, threads=options.threads, gzipped=TestFile.test_gzipped(database_obj.path_to_db))
        logger.debug('End writing taxon selected peptide database.')

    path = None
    # non redundant database
    if options.non_redundant:
        path = DatabaseCleaner.non_redundant(output_path, database_obj.is_with_taxon_id)
        # remove redundant database:
        output_path.unlink()

    if options.reduce_header and not database_obj.is_with_taxon_id:
        # reduce headers of NCBI database
        path = DatabaseCleaner.reduce_header(output_path)
        output_path.unlink()

    if options.gzip:
        path = str(path) if path is not None else str(output_path)
        if '_nr.fasta' in path:
            new_path = path.split('_nr.fasta')[0] + '.fasta'
            subprocess.run(['mv', path, new_path])
            path = new_path

        if '_rh.fasta' in path:
            new_path = path.split('_nr.fasta')[0] + '.fasta'
            subprocess.run(['mv', path, new_path])
            path = new_path

        subprocess.run(['gzip', '-f', path])
        logger.info("Fasta database is now gz compressed and saved to %s" % (path + '.gz'))
        # create config file
    if not options.config:
        try:
            path_to_main = Path(__file__, '..').resolve()
            with open(str(path_to_main / "tax2proteome.config"), 'w') as config:
                config.write('<database_folder>' + '\t' + str(database_obj.path_to_db_folder) + '\n')
                config.write('<database>' + '\t' + str(database_obj.db_type) + '\n')
                config.write('<path_to_database>' + '\t' + str(database_obj.path_to_db) + '\n')
                config.write('<taxon>' + '\t' + " ".join([str(taxid) for taxid in list(taxID_input)]) + '\n')
                config.write('<level>' + '\t' + str(options.level) + '\n')
                config.write('<out>' + '\t' + str(database_obj.output_path) + '\n')
                config.write('<threads>' + '\t' + str(options.threads) + '\n')
                config.write('<species>' + '\t' + str(int(options.species)) + '\n')
                config.write('<reduce_header>' + '\t' + str(int(options.reduce_header)) + '\n')
                config.write('<non_redundant>' + '\t' + str(int(options.non_redundant)) + '\n')
                config.write('<no_descendants>' + '\t' + str(int(options.no_descendants)) + '\n')
                config.write('<gzip>' + '\t' + str(int(options.gzip)) + '\n')
                config.write('<input>' + '\t' + str(options.input) + '\n')
                config.write('<column>' + '\t' + str(options.input) + '\n')
                config.write('<verbose>' + '\t' + str(int(options.verbose)) + '\n')
        except OSError:
            logger.debug('Can not create config file')

    logger.info('Program finished.')
    exit(0)


if __name__ == '__main__':
    main()
